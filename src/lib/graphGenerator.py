import numpy as np
from faker import Faker
import networkx as nx
import pandas as pd
import os
import gc
from .randomGenerator import RandomGenerator
from .dates.correction import DateCorrector,correct_dob_dod
from .location.correction import make_all_location_corrections
from .utils.utils import get_converters_pandas

class GraphGenerator():
    """
    Generate synthetic data and export it to an indicated folder.
    """
    def __init__(self,source_graph,links,base_name = 'SNDS',root=None,separator=','):
        """
        Parameters
        ----------
        source_graph: networkx.Graph object, the underlying structure of the database to generate
        links: pandas.DataFrame object, stores the links between all the tables of the database
        base_name: str, only 'OSCOUR' and 'SNDS' will trigger specific behavior.
        root: str, node of the graph to use as a root when converting the graph to a tree. Optional if base_name is in {OSCOUR,SNDS}
        """
        self.source_graph = source_graph
        self.links = links
        self.base_name = base_name
        if self.base_name == 'SNDS' and root is None:
            self.root = 'IR_BEN_R' #default root for SNDS
        elif self.base_name == 'OSCOUR' and root is None:
            self.root = 'OSCOUR' #default root for OSCOUR
        elif root is None:
            raise ValueError("Si le nom de la base n'est ni OSCOUR ni SNDS, il faut définir une root.")
        else:
            self.root = root
        self.tree = nx.bfs_tree(self.source_graph,self.root)
        if not nx.algorithms.tree.recognition.is_arborescence(self.tree):
            print('Impossible de procéder à la génération, la base doit être une arborescence (un seul parent par noeud)')
        #dictionnaire contenant les variables qui serviront à un moment de clé de jointure
        def collect_as_list(x):
            if not x.values[0] is None:
                s = '+'.join(x)
                return list(set(s.split('+')))
            else:
                return []
        if self.links.shape[0]>1:
            self.var_dict = self.links.groupby('source')['left_on'].apply(collect_as_list).to_dict()
        else:
            self.var_dict = {self.links['source'].iloc[0]:[]}
        self.df_store = {}
        self.separator = separator


    def sample(self,n_beneficiaires,path2output=''):
        """
        Generate data following an order induced from the tree structure of the database.
        Parameters
        ----------
        n_beneficiaires: int, number of patients to include in the database.
        path2output: str, folder where to export the tables once generated.
        """
        #initialisation
        to_treat = [self.root]
        treated = []
        while len(to_treat)>0:
            table = to_treat.pop(0)
            if table in treated:
                continue
            print(table)
            df = self.sample_structure(table,
                                       n_beneficiaires,
                                       path2output)
            df = self.sample_non_shared_variables(df,
                                                  table)
            if self.base_name == 'SNDS':
                date_corrector = DateCorrector(table,
                                               self.var_schemas,
                                               self.matching_ym,
                                               self.df_dates,
                                               self.matching_start_end)
                df = date_corrector.make_all_dates_corrections(df)
                df = make_all_location_corrections(df,
                                                  self.nomenclatures['FINESS'],
                                                  self.nomenclatures['IR_GEO_V'])
            #append child to a_traiter
            to_treat += self.tree._succ[table]
            #ecrire la table dans le bon dossier
            path2outputfile = os.path.join(path2output,self.table_path[table])
            dir,file = os.path.split(path2outputfile)
            if not os.path.exists(dir):
                os.makedirs(dir)
            df.to_csv(
                path2outputfile,
                sep=self.separator,
                index=None
            )
            #free up some memory
            del df
            gc.collect()


        #correct dates of birth and dates of death
        #if self.base_name == 'SNDS':
        #    correct_dob_dod(self.df_store,
        #                    self.df_dates,
        #                    self.var_dict,
        #                    self.tree,
        #                    self.links)



    def sample_structure(self,table,n_beneficiaires,path2output):
        """
        Randomly sample (or inherits) primary and foreign keys for a given table.

        Parameters
        ----------
        table: str, name of the table.
        n_beneficiaires: int, number of patients.
        path2output: str, folder where to export the tables once generated.

        Returns
        -------
        df: pandas.DataFrame, containing only variables which are primary or foreign keys for the table
        """
        variables = self.var_dict[table]
        pred = self.tree._pred[table]
        ##generate random variables and write file
        df = pd.DataFrame([])
        #repliquer les colonnes du parent si la table en a un, en corrigeant leur nom. Seule la root n'a pas de parent
        if pred:
            pred_table = list(pred.keys())[0]
            #print(pred_table)
            pred_df = pd.read_csv(os.path.join(path2output,self.table_path[pred_table]),
                                  dtype = get_converters_pandas(pred_table,self.var_schemas),
                                  sep=self.separator
                                 )
            #ajout de multiplicité des séjours et des consommations
            nb_repetitions = np.ones(pred_df.shape[0],
                                     dtype=int)
            if self.base_name=='SNDS':
                if (pred_table==self.root) and (table.startswith(('T_','ER_'))):
                    #idealement pas une uniforme mais une distribution du nombre d'hospit par patient/nb de conso par patient
                    nb_repetitions = np.random.randint(low=1,
                                                       high=10,
                                                       size=pred_df.shape[0])
            cols_list = self.source_graph.get_edge_data(table,
                                                        pred_table)
            cols_child_list = cols_list[table].split('+')
            cols_parent_list = cols_list[pred_table].split('+')
            for col_child,col_parent in zip(cols_child_list,cols_parent_list):
                df[col_child] = np.repeat(pred_df[col_parent].values,
                                          nb_repetitions)
            del pred_df
            gc.collect()
        variables_to_complete = list(set(variables).difference(set(df.columns)))
        for var in variables_to_complete:
            rg = RandomGenerator(self.var_schemas[table][var],
                                 self.nomenclatures,
                                 self.base_name)
            if table == self.root:
                df[var] = rg.sample(n_beneficiaires)
            else:
                df[var] = rg.sample(df.shape[0])

        if df.shape[0]==0:
            df['fake_column'] = np.arange(n_beneficiaires)

        return df


    def sample_non_shared_variables(self,df,table_name):
        """
        Randomly sample variables which are not primary or foreign keys.
        Parameters
        ----------
        df: pandas.DataFrame, partially filled table resulting from the sample_structure method.
        table_name: str, name of the table.

        Returns
        -------
        df: pandas.DataFrame, table with all columns filled.
        """

        for var_name in self.var_schemas[table_name]:
            if not var_name in df.columns:
                df[var_name] = RandomGenerator(self.var_schemas[table_name][var_name],
                                                                 self.nomenclatures).sample(df.shape[0])
        if 'fake_column' in df.columns:
            df.drop('fake_column',axis=1,inplace=True)

        return df
