import os
import pandas as pd
import json
import numpy as np
import networkx as nx

def find_path(table,extension):
    """
    Build the path to a given table in the SNDS
    Parameters
    ----------
    table: str, name of the table (ex: IR_BEN_R)
    extension: str, extension of the file concerned by the path (ex: .csv or .json)

    Returns
    -------
    table_path: str, path to the table with the right extension
    """
    #find product
    if table.startswith('T_'):
        product = 'PMSI_'+table.split('_')[1].split('aa')[0]
    elif table.startswith('ER_'):
        product = 'DCIR'
    elif table.startswith('IR_'):
        product = 'BENEFICIAIRE'
    elif table.startswith('CT_'):
        product = 'CARTOGRAPHIE_PATHOLOGIES'
    elif table.startswith('KI_'):
        product = 'Causes de décès'
    elif table in ['MA_REM_FT','OPEN_DAMIR']:
        product = 'DAMIR'
    elif table.startswith('EB_'):
        product = 'EGB'
    else:
        print(f'Table {table} not recognized')
    #find path to csv
    if not product.startswith('PMSI'):
        table_path = os.path.join(path,product+'/'+table+extension)
    else:
        table_path = os.path.join(path,'PMSI/')
        if product.split('_')[1]=='RIP':
            table_path = os.path.join(table_path,'PMSI RIM-P/'+table+extension)
        else:
            table_path = os.path.join(table_path,product.replace('_',' ')+'/'+table+extension)
    return table_path


def generate_links_table(path2schemas,name=None):
    """
    Parameters
    ----------
    path2schemas: str, path to the schema folder
    ignore_dcirs: bool, specific to the SNDS, whether to ignore the DCIRS branch or not.
    name: str, only useful if there is only one table.

    Returns
    -------
    links: pandas.DataFrame, contains the list of links induced from the schemas.
    """
    links = pd.DataFrame([],columns=['source','target','left_on','right_on'])
    #create list of all tables
    all_tables = []
    for root,dirs_list, files_list in os.walk(path2schemas):
        for file_name in files_list:
            if os.path.splitext(file_name)[-1] == '.json':
                all_tables.append(os.path.splitext(file_name)[0])

    #create table of links
    for root, dirs_list, files_list in os.walk(path2schemas):
        for file_name in files_list:
            if os.path.splitext(file_name)[-1] == '.json':
                file_name_path = os.path.join(root, file_name)
                desc = json.load(open(file_name_path))
                if 'foreignKeys' in desc:
                    n_links = 0
                    for d in desc['foreignKeys']:
                        if d['reference']['resource'] in all_tables:
                            left_on = d['fields'] if isinstance(d['fields'],str)\
                                                  else '+'.join(d["fields"])
                            right_on = d['reference']['fields']\
                                        if isinstance(d['reference']['fields'],str)\
                                        else '+'.join(d['reference']["fields"])
                            links = links.append(
                                {'source':file_name.split('.')[0],
                                 'target': d["reference"]['resource'],
                                 'left_on':left_on,
                                 'right_on':right_on},
                                 ignore_index=True
                            )
                            links = links.append(
                                {'target':file_name.split('.')[0],
                                 'source': d["reference"]['resource'],
                                 'right_on':left_on,
                                 'left_on':right_on},
                                 ignore_index=True
                            )
                            n_links += 1

    missing_tables = list(set(all_tables).difference(links.source.unique()))
    for table in missing_tables:
        links = links.append(
             {'source':table,
             'target': '',
             'left_on':'',
             'right_on':''},
             ignore_index=True
        )

    if links.shape[0]==0:
        links['source'] = [name]
        links = links.where(pd.notnull(links),None)
    return links


def generate_graph_from_links(links,save_path=None):
    """
    Generates an adjacency matrix then a graph from a links dataframe.
    Parameters
    ----------
    links: pandas.DataFrame, typically resulting from a call to `generate_links_table`
    save_path: None or str, if string then indicates where to save the resulting graph. It will be saved as a pickle (.pkl) file.

    Returns
    -------
    G: networkx.Graph object, the graph built from the links table.
    """
    all_tables = list(set(links['source'].unique()).union(set(links['target'].unique())))
    all_tables = [t for t in all_tables if not t == '']
    G = nx.Graph()
    G.add_nodes_from(all_tables)
    for source, target, left_on,right_on in zip(links['source'],
                                                links['target'],
                                                links['left_on'],
                                                links['right_on']):
        if not target == '':
            G.add_edge(source,target)
            G[source][target][source] = left_on
            G[source][target][target] = right_on

    if not save_path is None:
        nx.write_gpickle(G,save_path)
    return G


def get_converters_pandas(table_name,var_schemas):
    """
    Get type for each field in a given table.
    The result is a dict, so that the command
    `pandas.read_csv(table_name.csv,
                     dtype=get_converters_pandas(table_name,var_schemas))`
    will return a pandas.DataFrame with all its columns in the right dtype.

    Parameters
    ----------
    table_name: str, name of the table
    var_schemas: dict, of the form dict[table][variable] = description

    Returns
    -------
    converters: dict, of the form dict[var] = type
    """
    converters = {k:v['type'] for k,v in var_schemas[table_name].items()}
    string_types = ['date','datetime','yearmonth']
    int_types = ['month','year','integer']
    for var, type in converters.items():
        if type in string_types:
            converters[var] = 'string'
        elif type in int_types:
            converters[var] = 'int64'
        elif type == 'number':
            converters[var] = 'float'
    if 'AGE_JOU' in converters:
        converters.pop('AGE_JOU')
    return converters
