import numpy as np
from faker import Faker

class RandomGenerator():
    """
    Randomly samples columns for a given nomenclature.
    """
    fake = Faker()

    def __init__(self,desc,nomenclatures=None,base_name='SNDS'):
        """
        Parameters
        ----------
        desc: dict, description from the schema files
        nomenclatures: dict, gives all the nomenclatures with their name.
        """
        self.desc = desc
        self.data_type = desc["type"]
        implemented_types = ['string','float','integer','date','boolean','year','month','yearmonth','datetime','number']
        self.nomenclatures = nomenclatures
        self.base_name = base_name
        if not self.data_type in implemented_types:
            raise NotImplemented

    def sample(self,n_samples):
        """
        Generates samples.
        Parameters
        ----------
        n_samples: int, number of samples to generate.

        Returns
        -------
        col: list or numpy.array of shape (n_samples,)
        """

        if "possible_values" in self.desc:
            col = np.random.choice(self.desc["possible_values"],n_samples)

        elif (self.desc["nomenclature"]=='-') or (self.desc["nomenclature"]=='IR_DTE_V' and self.base_name=='SNDS'):
            if self.data_type=='string':

                length = int(self.desc["length"]) if self.desc["length"] else None
                col = [self.fake.pystr(min_chars=length, max_chars=length) for _ in range(n_samples)]
                # correct nans
                if length==3:
                    col = ['nun' if (x.lower()=='nan') else x for x in col]

            elif self.data_type=='float':
                col = np.random.uniform(0,1e4,n_samples)

            elif self.data_type=='integer' or self.data_type=='number':

                if "constraints" in self.desc:
                    col = np.random.randint(low=self.desc["constraints"]["minimum"],
                                            high=self.desc["constraints"]["maximum"]+1,
                                            size=n_samples,
                                           dtype=np.int64)

                elif self.desc["length"]:
                    col = np.random.randint(low=0,high=10**(self.desc["length"]),size=n_samples,dtype=np.int64)
                else:
                    col = np.random.randint(low = 1,high=int(1e3),size=n_samples)


            elif self.data_type in ['date','datetime']:
                if self.desc['format']=='default':
                    col = [self.fake.date(pattern='%d%b%Y:00:00:00',end_datetime=None).upper() for _ in range(n_samples)]
                else:
                    col = [self.fake.date(pattern=self.desc["format"], end_datetime=None) for _ in range(n_samples)]
            elif self.data_type=='boolean':
                col = [self.fake.pybool() for _ in range(n_samples)]
            elif self.data_type=='year':
                y_min = 1960
                y_max = 2020
                if "constraints" in self.desc:
                    y_min = self.desc["constraints"]["minimum"]
                    y_max = self.desc["constraints"]["maximum"]
                col = np.random.randint(y_min,y_max,size=n_samples)
            elif self.data_type=='month':
                col = [self.fake.date(pattern=self.desc["format"], end_datetime=None) for _ in range(n_samples)]
            elif self.data_type=='yearmonth':
                col = [self.fake.date(pattern='%Y%m', end_datetime=None) for _ in range(n_samples)]
            else:
                raise NotImplemented

        else: #if nomenclature in list of nomenclatures
            var_name_in_nomenc = self.desc["name"]
            if ":" in self.desc["nomenclature"]:
                nomenc = self.desc["nomenclature"].split(":")[0]
                var_name_in_nomenc = self.desc["nomenclature"].split(":")[-1]
                possible_values = self.nomenclatures[nomenc][var_name_in_nomenc].values
            else:
                nomenc = self.desc["nomenclature"]
                possible_values = self.nomenclatures[nomenc].iloc[:,0].values
            if 'proba' in self.nomenclatures[nomenc].columns:
                p = self.nomenclatures[nomenc]["proba"]
            elif 'proba_REL' in self.nomenclatures[nomenc] and 'REL' in var_name_in_nomenc: #diagnostic relié
                p = self.nomenclatures[nomenc]['proba_REL']
            elif 'proba_ASS' in self.nomenclatures[nomenc] and 'ASS' in var_name_in_nomenc: #diagnostic associé
                p = self.nomenclatures[nomenc]['proba_ASS']

            elif 'proba_PAL' in self.nomenclatures[nomenc] and 'PAL' in var_name_in_nomenc: #diagnostic principal ou non précisé
                p = self.nomenclatures[nomenc]['proba_PAL']
            else:
                p = None

            col = np.random.choice(possible_values,
                                   size= n_samples,
                                   p=p,
                                   replace=True)

        return col
