import os
import argparse
import warnings
from lib.process import Process

warnings.simplefilter('ignore')

def arg_parsing():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c',"--config",type=str, help="chemin vers le fichier de configuration")
    args = parser.parse_args()
    return args


if __name__=='__main__':

    args = arg_parsing()
    process = Process(args.config)
    process.build_resources()
    process.build_metagenerator()
    process.run()
