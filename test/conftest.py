import pytest

def pytest_configure():
    pytest.metagraphGenerator = None
    pytest.graphGenerator = None
