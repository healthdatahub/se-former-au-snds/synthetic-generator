import pytest
from .conf import process_test
from src.lib.utils.utils import generate_links_table,generate_graph_from_links
import pandas as pd
import networkx as nx
from src.lib.metagraphGenerator import MetaGraphGenerator
import os

process = process_test

def test_generate_links_table(process):
    links = generate_links_table(process.path2schemas)
    assert isinstance(links,pd.DataFrame)
    assert set(links.columns) == set(process.links.columns)
    assert links.equals(process.links)

def test_generate_graph_from_links(process):
    graph = generate_graph_from_links(process.links)
    assert isinstance(graph,nx.Graph)
    assert graph._node == process.graph._node
    assert graph._adj == process.graph._adj

def test_build_var_schemas(process):
    pytest.metagraphGenerator = MetaGraphGenerator(process.graph,
                                                   process.links,
                                                   process.base_name,
                                                   process.roots)
    pytest.metagraphGenerator.build_var_schemas(process.path2schemas)
    assert isinstance(pytest.metagraphGenerator.var_schemas,dict)
    assert set(pytest.metagraphGenerator.var_schemas.keys())==set(process.var_schemas.keys())
    assert pytest.metagraphGenerator.var_schemas == process.var_schemas

def test_build_var_schemas_modifier(process):
    pytest.metagraphGenerator.build_var_schemas(process.path2schemas,
                                                process.schema_modifiers)
    assert isinstance(pytest.metagraphGenerator.var_schemas,dict)
    assert set(pytest.metagraphGenerator.var_schemas.keys())==set(process.modified_var_schemas.keys())
    assert pytest.metagraphGenerator.var_schemas == process.modified_var_schemas
    #back to normal
    pytest.metagraphGenerator.build_var_schemas(process.path2schemas,[])

def test_build_nomenclatures(process):
    pytest.metagraphGenerator.build_nomenclatures(process.path2nomenc)
    assert isinstance(pytest.metagraphGenerator.nomenclatures,dict)
    assert set(pytest.metagraphGenerator.nomenclatures.keys())==set(process.nomenclatures.keys())
    for k in process.nomenclatures.keys():
        assert pytest.metagraphGenerator.nomenclatures[k].equals(process.nomenclatures[k])

def test_build_dates_matching(process):
    pytest.metagraphGenerator.build_dates_matching(process.path2schemas)
    assert set(pytest.metagraphGenerator.df_dates.columns) == set(process.df_dates.columns)
    assert set(pytest.metagraphGenerator.matching_start_end.columns) == set(process.matching_start_end.columns)
    assert set(pytest.metagraphGenerator.matching_ym.columns) == set(process.matching_ym.columns)
    assert pytest.metagraphGenerator.df_dates.equals(process.df_dates)
    assert pytest.metagraphGenerator.matching_start_end.equals(process.matching_start_end)
    non_null_size = (process.matching_ym.shape[0]>0)
    pd.testing.assert_frame_equal(pytest.metagraphGenerator.matching_ym,
                                  process.matching_ym,
                                  check_index_type=False,
                                  check_dtype=non_null_size)

def test_build_table_path(process):
    pytest.metagraphGenerator.build_table_path(process.path2schemas)
    assert set(pytest.metagraphGenerator.table_path.keys()) == set(process.table_path.keys())

def test_sample(process):
    pytest.metagraphGenerator.push_to_subgenerators()
    pytest.graphGenerator = [sg for sg in pytest.metagraphGenerator.subgenerators
                    if len(sg.source_graph)==process.size_largest_component][0]
    pytest.graphGenerator.sample(process.n_beneficiaires,
                                 process.output_dir)
    assert os.listdir(process.output_dir)==process.list_files_single_graph
    df = pd.read_csv(os.path.join(process.output_dir,process.roots[0]+'.csv'))
    assert df.shape[0] == process.n_beneficiaires
