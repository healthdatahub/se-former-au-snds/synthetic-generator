import pandas as pd
import networkx as nx
import json
import os
import pytest
import pathlib
from src.lib.schema_modifier import SchemaModifier

class ProcessTest():

    dir_work = pathlib.Path(__file__).parent

    def __init__(self):
        self.n_beneficiaires = 10
        self.path2schemas = os.path.join(self.dir_work,'resources/schemas')
        self.links = pd.read_csv(os.path.join(self.dir_work,'resources/links_test.csv'),keep_default_na=False)
        self.graph = nx.read_gpickle(os.path.join(self.dir_work,'resources/graph_test.pkl'))
        self.base_name = 'TEST'
        self.roots = ['tableA']
        self.var_schemas = json.load(open(os.path.join(self.dir_work,'resources/var_schemas.json')))
        self.modified_var_schemas = json.load(open(os.path.join(self.dir_work,'resources/modified_var_schemas.json')))
        self.schema_modifiers = [SchemaModifier('tableB|col3|type|string')]
        self.path2nomenc = os.path.join(self.dir_work,'resources/nomenclatures')
        self.nomenclatures = {
        'nomencA':pd.read_csv(os.path.join(self.dir_work,'resources/nomenclatures/nomencA.csv'),
                              sep=';')
                              }
        self.df_dates = pd.read_csv(os.path.join(self.dir_work,'resources/df_dates.csv'),keep_default_na=False)
        self.matching_start_end = pd.read_csv(os.path.join(self.dir_work,'resources/matching_start_end.csv'))
        self.matching_ym = pd.read_csv(os.path.join(self.dir_work,'resources/matching_ym.csv'))
        self.size_largest_component = 3
        self.output_dir = os.path.join(self.dir_work,'output')
        self.table_path = json.load(open(os.path.join(self.dir_work,'resources/table_path.json')))
        self.list_files_single_graph = [
                                        'tableA.csv',
                                        'tableB.csv',
                                        'tableC.csv'
        ]




@pytest.fixture
def process_test():
    return ProcessTest()
